import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  mounted() {
    // this.axios.get("https://jsonplaceholder.typicode.com/users")
    //     .then(res => {
    //       const users = res.data
    //       users.forEach(u=>console.log(u.name))
    //     })
      this.$store.dispatch('loadNames')
  }
}).$mount('#app')
