import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)

Vue.use(VueAxios, axios)

export default new Vuex.Store({
    state: {
        names: []
    },
    mutations: {
        SET_NAME(state, name) {
            state.names.push(name)
        }
    },
    actions: {
        loadNames({commit}) {
            axios
                .get("https://jsonplaceholder.typicode.com/users")
                .then(res => res.data)
                .then(users => {
                    users.forEach((user) => {
                        const name = user.name.toUpperCase()
                        commit('SET_NAME', name)
                    })
                })
        }
    },
    getters: {
        allTerms: state => state.names
    }
})
